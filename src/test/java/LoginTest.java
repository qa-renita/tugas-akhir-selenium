import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;

public class LoginTest {
    WebDriver driver;


    @BeforeClass
    public void setup() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @AfterMethod
    public void logout() throws InterruptedException {
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/authenticate.php?logout");
        Thread.sleep(1000);
    }

    @Test(priority = 0)
    public void TC001() {
        //ke url login
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");
        //ambil value username dan password
        String username = driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[2]/form/div[1]/div[1]/div/div/input")).getAttribute("value");
        String password = driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[2]/form/div[1]/div[2]/div/div/input")).getAttribute("value");
        //login
        driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).sendKeys(username);
        driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).sendKeys(password);
        driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();

        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"appointment\"]/div/div/div/h2")).getText(), "Make Appointment");
    }

    @Test(priority = 1)
    public void TC002() {
        //ke url login
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");
        //login
        driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).sendKeys("John Doe");
        driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).sendKeys("passwordSalah");
        driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();

        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[1]/p[2]")).getText(), "Login failed! Please ensure the username and password are valid.");
    }

    @Test(priority = 2)
    public void TC003() {
        //ke url login
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");
        //login
        driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).sendKeys("Username Salah");
        driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).sendKeys("ThisIsNotAPassword");
        driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();

        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[1]/p[2]")).getText(), "Login failed! Please ensure the username and password are valid.");
    }

    @Test(priority = 3)
    public void TC004() {
        //ke url login
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");
        //login
        driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).sendKeys("Username salah");
        driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).sendKeys("passwordSalah");
        driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();

        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[1]/p[2]")).getText(), "Login failed! Please ensure the username and password are valid.");
    }

    @DataProvider(name = "data-provider")
    public Object[][] dpMethod() {
        return new Object[][]{{"John Doe", "ThisIsNotAPassword", "success"}
                , {"Username salah", "ThisIsNotAPassword", "failed"}
                , {"John Doe", "PasswordSalah", "failed"}
                , {"Username salah", "PasswordSalah", "failed"}
        };
    }

    @Test(priority = 4, dataProvider = "data-provider")
    public void TC005(String dataUsername, String dataPassword, String result) {
        //ke url login
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");
        //ambil value username dan password
        System.out.println(dataUsername);
        String username = dataUsername;
        System.out.println(dataPassword);
        String password = dataPassword;
        //login
        driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).sendKeys(username);
        driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).sendKeys(password);
        driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();

        if (result.equals("success")) {
            Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"appointment\"]/div/div/div/h2")).getText(), "Make Appointment");
        } else {
            Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[1]/p[2]")).getText(), "Login failed! Please ensure the username and password are valid.");
        }
    }

    @AfterClass
    public void closeBrowser() throws InterruptedException {
        Thread.sleep(2000);
        driver.quit();
    }
}
